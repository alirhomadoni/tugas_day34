<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
      User::create([
       'name' => request('name'),
       'nim' => request('nim'),
       'email'=>request('email'),
       'password'=>bcrypt(request('password')),
       'fakultas'=>request('fakultas'),
       'jurusan'=>request('jurusan'),
       'no_hp'=>request('no_hp'),
       'no_wa'=>request('no_wa'),
       'role_id'=>request('role_id'),

     ]);
     return response('thaks for register');
 }
}
