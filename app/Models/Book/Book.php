<?php

namespace App\Models\Book;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    //
    protected $fillable=[
      'user_id','book_id','tanggal_pinjam','batas_akhir_pinjam','isOntime',
    ];

    public function users(){
      return $this->belongsToMany(User::class)->using('pinjams');
    }
}
