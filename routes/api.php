<?php

Route::post('register','Auth\RegisterController');
Route::post('login','Auth\LoginController');
Route::post('logout','Auth\LogoutController');

Route::get('user','UserController');
